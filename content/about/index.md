+++
title = "About Me"
date = 2018-06-03T10:41:27-04:00
+++

{% aside() %}
This page is about me, Daniel Sockwell, the person.  If you're interested in details about this site, there’s a [separate page for that](../projects) (or you can just [check out the source code for this site](https://gitlab.com/codesections/codesections-website)).
{% end %}

I’m Daniel, and I’m on a mission to bridge the gap between law and
technology.  Before embarking on this project, I  was an attorney at a major
New York law firm, where my practice focused on civil litigation and
emphasized cybersecurity, data privacy, and other intersections between law
and technology.

I grew up in Birmingham, Alabama, and have also lived in California,
Mississippi, New York, North Carolina, and Texas.  When I’m not working on
programming or legal topics, you can find me out hiking in the backcountry,
reading a science fiction book, or playing a board game with my wife ([Race for
the Galaxy](https://boardgamegeek.com/boardgame/28143/race-galaxy) is a 
current favorite).

## Contact 
Please feel free to reach out to me using any of the methods below.  Please
note that, while I maintain placeholder profiles on Facebook and Twitter, I do
not use either of those services—instead, I use
[Mastodon](https://joinmastodon.org/) as my primary social network.

| Contact Method | Link                          |
|:--------|:------------------------------------------------------------------|
| Mastodon| [fosstodon.org/@codesections](https://fosstodon.org/@codesections)|
| GitLab  | [gitlab.com/codesections](https://gitlab.com/codesections)        |
| GitHub  | [github.com/codesections](https://github.com/codesections)        |
| LinkedIn| [linkedin.com/in/daniel-sockwell](https://www.linkedin.com/in/daniel-sockwell) |
| KeyBase | [keybase.io/codesections](https://keybase.io/codesections)        |
| Email   | <daniel@codesections.com>                                         |
|Public Key| [codesections.com/public-key.txt](https://www.codesections.com/public-key.txt) |
