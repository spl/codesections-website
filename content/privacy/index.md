+++
title = "Privacy Guarantees"
date = 2018-06-03T10:41:27-04:00
+++

This site does not use cookies, JavaScript analytics (Google or otherwise) or
track user data in any other way.  If you would like me to know that you read
something here, send me an email or let me know on Mastodon, because I won't 
know in any other way. 
